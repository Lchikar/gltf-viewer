#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightingDirection; 
uniform vec3 uLightingIntensity;

out vec3 fColor;

void main()
{
	float pi = 3.14;
	vec3 bdrf = vec3(1/pi, 1/pi, 1/pi);
	vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
	fColor = bdrf * uLightingIntensity * dot(viewSpaceNormal, uLightingDirection);

}