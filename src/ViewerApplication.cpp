#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>
#include "utils/gltf.hpp"
#include "utils/images.hpp"


const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

bool ViewerApplication::loadGltfFile(tinygltf::Model & model) {
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());
  // bool ret = loader.LoadBinaryFromFile(&model, &err, &warn, argv[1]); //
  // for binary glTF(.glb)

  if (!warn.empty()) {
      printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
      printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
      printf("Failed to parse glTF\n");
      return false;
  }
  return true;
}

std::vector<GLuint> ViewerApplication::createBufferObjects( const tinygltf::Model &model){
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);
  glGenBuffers((GLsizei)model.buffers.size(), bufferObjects.data());
  for (size_t i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(), model.buffers[i].data.data(), 0);
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0); // Cleanup the binding point after the loop only

  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects( const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> & meshIndexToVaoRange){
 
  std::vector<GLuint> vertexArrayObjects;

  meshIndexToVaoRange.resize(model.meshes.size());

  for(auto meshIter = model.meshes.begin(); meshIter != model.meshes.end(); meshIter++){
    const auto vaoOffset = (GLsizei)vertexArrayObjects.size();
    const auto primitiveSize = (GLsizei)meshIter->primitives.size();
    vertexArrayObjects.resize(vaoOffset + primitiveSize);
    meshIndexToVaoRange.push_back(VaoRange{vaoOffset, primitiveSize});
    
    glGenVertexArrays(primitiveSize, &(vertexArrayObjects[vaoOffset]));

    for(int primitiveIdx = 0; primitiveIdx < meshIter->primitives.size(); primitiveIdx++){
      glBindVertexArray(vertexArrayObjects[vaoOffset + primitiveIdx]);

      // POSITION
      { // I'm opening a scope because I want to reuse the variable iterator in the code for NORMAL and TEXCOORD_0
        auto primitive = meshIter->primitives[primitiveIdx];
        const auto iterator = primitive.attributes.find("POSITION");
        if (iterator != end(primitive.attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; // TODO get the correct tinygltf::Accessor from model.accessors
          const auto &bufferView = model.bufferViews[accessor.bufferView]; // TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
          const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

          const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);

          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view
          
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, 3, GL_FLOAT, GL_FALSE, (GLsizei)(bufferView.byteStride), (const GLvoid*)byteOffset);
          // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
          // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
        }
      }
      
      // NORMAL
      { // I'm opening a scope because I want to reuse the variable iterator in the code for NORMAL and TEXCOORD_0
        auto primitive = meshIter->primitives[primitiveIdx];
        const auto iterator = primitive.attributes.find("NORMAL");
        if (iterator != end(primitive.attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; // TODO get the correct tinygltf::Accessor from model.accessors
          const auto &bufferView = model.bufferViews[accessor.bufferView]; // TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
          const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

          const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_NORMAL_IDX which has to be defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);

          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view
          
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, 3, GL_FLOAT, GL_FALSE, (GLsizei)(bufferView.byteStride), (const GLvoid*)byteOffset);
          // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
          // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
        }
      }
      
      // TEXTURE COORDINATES
      { // I'm opening a scope because I want to reuse the variable iterator in the code for NORMAL and TEXCOORD_0
        auto primitive = meshIter->primitives[primitiveIdx];
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        if (iterator != end(primitive.attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; // TODO get the correct tinygltf::Accessor from model.accessors
          const auto &bufferView = model.bufferViews[accessor.bufferView]; // TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
          const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

          const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_TEXCOORD0_IDX which has to be defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);

          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view
          
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, 3, GL_FLOAT, GL_FALSE, (GLsizei)(bufferView.byteStride), (const GLvoid*)byteOffset);
          // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
          // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
        }
      }

      if(meshIter->primitives[primitiveIdx].indices >= 0){
        const auto accessorIdx = meshIter->primitives[primitiveIdx].indices;
        const auto &accessor = model.accessors[accessorIdx]; 
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;
        const auto bufferObject = bufferObjects[bufferIdx];

        assert(GL_ELEMENT_ARRAY_BUFFER == bufferView.target);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject);
      }
    } 
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const{
  std::vector<GLuint> textures(model.textures.size(), 0);

  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  glGenTextures(GLsizei(model.textures.size()), textures.data());


  for(auto i = 0; i < model.textures.size(); i++){
    const auto &texture = model.textures[i]; // get i-th texture
    assert(texture.source >= 0); // ensure a source image is present

    const auto &image = model.images[texture.source]; // get the image

    // Bind texture object to target GL_TEXTURE_2D:
    glBindTexture(GL_TEXTURE_2D, textures[i]);

    // fill the texture object with the data from the image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());
    
    const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    // Case where sampler use mipmapping for minification filter
    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST || sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR || sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST || sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
    
    glBindTexture(GL_TEXTURE_2D, 0);

  }

  return textures;
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram =
      compileProgram({m_ShadersRootPath / m_vertexShader,
          m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  const auto lightDirectionLocation = glGetUniformLocation(glslProgram.glId(), "uLightingDirection");
  const auto lightIntensityLocation = glGetUniformLocation(glslProgram.glId(), "uLightingIntensity");

  const auto baseColorTextureLocation = glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  
  glm::vec3 lightDirection;
  glm::vec3 lightIntensity;
  bool lightCam = false;

  tinygltf::Model model;
  // TODO Loading the glTF file
  loadGltfFile(model);

  // What is the formula for the center point of the bounding box defined by
  // bboxMin and bboxMax ? centerPoint = (bboxMin + bboxMax)/2

  // What is the formula for the diagonal vector going from bboxMin to bboxMax ?
  // vectorBboxMinToBboxMax = bboxMax - bboxMin

  glm::vec3 bboxMin;
  glm::vec3 bboxMax;

  computeSceneBounds(model, bboxMin, bboxMax);
  

  // Build projection matrix
  auto maxDistance = glm::length(bboxMax - bboxMin); // TODO use scene bounds instead to compute this
  maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.3f * maxDistance);

  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera
    //cameraController.setCamera( Camera{glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0)});
    const auto center = glm::vec3(.5f*(bboxMin + bboxMax));
    const auto diagonal = glm::vec3(bboxMax - bboxMin);
    const auto up = glm::vec3(0, 1, 0);
    const auto eye = diagonal.z == 0 ? center + diagonal : center + 2.f * glm::cross(diagonal, up);
    
    cameraController->setCamera(Camera{eye, center, up});
  }

  //  What is a condition to test if the scene is flat on the z axis ?
  // test if diagonal.z = 0

  // Texture Objects Creation
  std::vector<GLuint> textureObjects = createTextureObjects(model);
  GLuint whiteTexture = 0;
  float white[] = {1, 1, 1, 1};
  // Generate the texture object:
  glGenTextures(1, &whiteTexture);

  // Bind texture object to target GL_TEXTURE_2D:
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  // Set image data:
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGB, GL_FLOAT, white);
  // Set sampling parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // Set wrapping parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);

  glBindTexture(GL_TEXTURE_2D, 0);

  // TODO Creation of Buffer Objects
  const auto bufferObjects = createBufferObjects(model);

  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshToVertexArrays;
  const auto vertexArrayObjects = createVertexArrayObjects(model, bufferObjects, meshToVertexArrays);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  const auto bindMaterial = [&](const auto materialIndex) {
    // Material binding
    if (materialIndex >= 0) {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      if (baseColorTextureLocation >= 0) {
        auto textureObject = whiteTexture;

        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];

          if (texture.source >= 0) {
            textureObject = textureObjects[pbrMetallicRoughness.baseColorTexture.index];
          }
        }

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(baseColorTextureLocation, 0);
      }
    } else {
      // Apply default material
      // Defined here:
      // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#reference-material
      // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#reference-pbrmetallicroughness3
      if (baseColorTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(baseColorTextureLocation, 0);
      }
    }
  };


  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    if (lightDirectionLocation >= 0) {
        if (lightCam) {
            glUniform3f(lightDirectionLocation, 0, 0, 1);
        }
        else {
            const auto lightDirectionNormView = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0)));
            glUniform3f(lightDirectionLocation, lightDirectionNormView[0], lightDirectionNormView[1], lightDirectionNormView[2]);
        }
    }
    if (lightIntensityLocation >= 0) {
        const auto lightIntensityNormView = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightIntensity, 0)));
        glUniform3f(lightIntensityLocation, lightIntensityNormView[0], lightIntensityNormView[1], lightIntensityNormView[2]);
    }


    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          // TODO The drawNode function
          const auto node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          if (node.mesh >= 0) {
            const auto modelViewMatrix = viewMatrix * modelMatrix;
            const auto modelViewProjectionMatrix = projMatrix * modelViewMatrix;
            const auto normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));

            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));
          
            const auto mesh = model.meshes[node.mesh];
            const auto vaoRange = meshToVertexArrays[node.mesh];

            for (auto primIdx = 0; primIdx < mesh.primitives.size(); primIdx++) {
              bindMaterial(mesh.primitives[primIdx].material);

              const auto vao = vertexArrayObjects[vaoRange.begin + primIdx];
              glBindVertexArray(vao);

              const auto primitive = mesh.primitives[primIdx];

              if (primitive.indices >= 0) {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset = bufferView.byteOffset;

                glDrawElements(primitive.mode, (GLsizei)accessor.count, accessor.componentType, (const GLvoid *)byteOffset);
              }
              else {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];

                glDrawArrays(primitive.mode, 0, (GLsizei)accessor.count);
              }

              for (auto childIdx = 0; childIdx < node.children.size(); childIdx++) {
                drawNode(node.children[childIdx], modelMatrix);
              }
            }

          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {

      // TODO Draw all nodes      
      const auto nodes = model.scenes[model.defaultScene].nodes;
      for (int nodeIdx = 0; nodeIdx < (int)nodes.size(); nodeIdx ++) {
        drawNode(nodeIdx, glm::mat4(1));
      }
      
    }
  };

  if (!m_OutputPath.empty()) {
    const auto numComponents = 3;
    std::vector<unsigned char> pixels((GLsizei)numComponents * m_nWindowWidth * m_nWindowHeight);

    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() { drawScene(cameraController->getCamera()); });

    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data());

    const auto strPath = m_OutputPath.string();
    stbi_write_png(
        strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);

    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }
      }

      if (ImGui::CollapsingHeader("Camera type", ImGuiTreeNodeFlags_DefaultOpen)) {
          static int cameraType = 0;

          if (ImGui::RadioButton("Trackball camera", &cameraType, 0)) {
              cameraController = std::make_unique<TrackballCameraController>(
                  m_GLFWHandle.window(), 0.3f * maxDistance);

              cameraController->setCamera(cameraController->getCamera());
          }
          ImGui::SameLine();

          if (ImGui::RadioButton("First Person camera", &cameraType, 1)) {
              cameraController = std::make_unique<FirstPersonCameraController>(
                  m_GLFWHandle.window(), 0.3f * maxDistance);

              cameraController->setCamera(cameraController->getCamera());
          }
      }

      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
          bool hasChanged;

          static float theta = 0.0f;
          hasChanged = ImGui::SliderFloat("Theta", &theta, 0, glm::pi<float>(), "%.1f");

          static float phi = 0.0f;
          hasChanged = hasChanged || ImGui::SliderFloat("phi", &phi, 0, 2*glm::pi<float>(), "%.1f");

          if (hasChanged) {
              lightDirection = glm::vec3(glm::sin(theta) * glm::cos(phi), glm::cos(phi), glm::sin(theta) * glm::sin(phi));
          }


          static glm::vec3 color(1,1,1);
          static float intensity = 1;
          hasChanged = ImGui::ColorEdit3("Color", (float*)&color);
          hasChanged = hasChanged || ImGui::InputFloat("Intensity", &intensity);

          if (hasChanged) {
              lightIntensity = color * intensity;
          }

      }

      ImGui::Checkbox("Light from camera", &lightCam);

      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
